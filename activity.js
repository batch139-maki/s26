// The questions are as follows:
// - What directive is used by Node.js in loading the modules it needs?
// ANSWER: require()


// - What Node.js module contains a method for server creation?
// ANSWER: HTTP module


// - What is the method of the http object responsible for creating a server using Node.js?
// ANSWER: createServer()


// - What method of the response object allows us to set status codes and content types?
// ANSWER: response.writeHEAD


// - Where will console.log() output its contents when run in Node.js?
// ANSWER: Terminal/gitbash

// - What property of the request object contains the address's endpoint?
// ANSWER: response.end
